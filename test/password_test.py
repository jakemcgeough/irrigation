from argon2 import PasswordHasher
from argon2.exceptions import VerificationError, VerifyMismatchError, HashingError, InvalidHash


# first set text in password.txt to hash of awesomepassword


def password_test():
	ph = PasswordHasher()

	pw = 'awesomepassword'

	# password_hash = ph.hash(pw)

	with open('../../password.txt', 'r') as f:
		f_password = f.read()

	try:
		ph.verify(f_password, pw)
		print("Verified!")

	except (VerificationError, VerifyMismatchError, HashingError, InvalidHash):
		print("Error")

	# except VerifyMismatchError:
	# 	print("VerifyMismatchError")

	# except HashingError:
	# 	print("HashingError")

	# except InvalidHash:
	# 	print("InvalidHash")
	

if __name__ == "__main__":
	password_test()

