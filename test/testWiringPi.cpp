#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>
#include <thread>
#include <string>
#include <utility>
#include <set>
#include <vector>


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include <wiringPi.h>

using namespace std;

// GPIO:
const int z1 = 21; // pin 29
const int z2 = 22; // pin 31
const int z3 = 23; // pin 33
const int z4 = 27; // pin 36
const int z5 = 28; // pin 38
const int z6 = 29; // pin 40

int main() {
	wiringPiSetup();
	// pinMode(0, OUTPUT);
	// pinMode(1, OUTPUT);
	// pinMode(2, OUTPUT);
	// pinMode(3, OUTPUT);
	// pinMode(4, OUTPUT);
	// pinMode(5, OUTPUT);


	vector<int> pins = {21, 22, 23, 24, 25, 27, 28, 29};

	for (auto i : pins) {
		pinMode(i, OUTPUT);
	}

	for (auto i : pins) {
		digitalWrite(i, 1);
	}

	digitalWrite(z6, 1);
	std::this_thread::sleep_for(std::chrono::seconds(5));
	digitalWrite(z6, 0);
	std::this_thread::sleep_for(std::chrono::seconds(5));

	digitalWrite(z6, 1);
	std::this_thread::sleep_for(std::chrono::seconds(5));
	digitalWrite(z6, 0);
	std::this_thread::sleep_for(std::chrono::seconds(5));

	digitalWrite(z6, 1);
	std::this_thread::sleep_for(std::chrono::seconds(5));
	digitalWrite(z6, 0);
	// std::this_thread::sleep_for(std::chrono::seconds(5));

	// digitalWrite(z5, 0);
	// std::this_thread::sleep_for(std::chrono::seconds(5));
	// digitalWrite(z5, 1);
	// std::this_thread::sleep_for(std::chrono::seconds(5));

	// digitalWrite(z5, 0);
	// std::this_thread::sleep_for(std::chrono::seconds(5));
	// digitalWrite(z5, 1);
	// std::this_thread::sleep_for(std::chrono::seconds(5));

	// digitalWrite(z5, 0);
	// std::this_thread::sleep_for(std::chrono::seconds(5));
	// digitalWrite(z5, 1);

	return 0;
}