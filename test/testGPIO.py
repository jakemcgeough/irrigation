import RPi.GPIO as GPIO
import time

def testGPIO():
	GPIO.setmode(GPIO.BCM)

	GPIO.setup(5, GPIO.OUT)
	GPIO.setup(6, GPIO.OUT)
	GPIO.setup(13, GPIO.OUT)
	GPIO.setup(16, GPIO.OUT)
	GPIO.setup(20, GPIO.OUT)
	GPIO.setup(21, GPIO.OUT)

	GPIO.output(5, GPIO.HIGH)
	GPIO.output(6, GPIO.HIGH)
	GPIO.output(13, GPIO.HIGH)
	GPIO.output(16, GPIO.HIGH)
	GPIO.output(20, GPIO.HIGH)
	GPIO.output(21, GPIO.HIGH)

	GPIO.output(5, GPIO.LOW)
	time.sleep(3)
	GPIO.output(5, GPIO.HIGH)

	GPIO.output(6, GPIO.LOW)
	time.sleep(3)
	GPIO.output(6, GPIO.HIGH)

	GPIO.output(13, GPIO.LOW)
	time.sleep(3)
	GPIO.output(13, GPIO.HIGH)

	GPIO.output(16, GPIO.LOW)
	time.sleep(3)
	GPIO.output(16, GPIO.HIGH)

	GPIO.output(20, GPIO.LOW)
	time.sleep(3)
	GPIO.output(20, GPIO.HIGH)

	GPIO.output(21, GPIO.LOW)
	time.sleep(3)
	GPIO.output(21, GPIO.HIGH)

	GPIO.cleanup()

if __name__ == '__main__':
	testGPIO()