import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

GPIO.setup(21, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(28, GPIO.OUT)
GPIO.setup(29, GPIO.OUT)

state1 = GPIO.input(21)
state2 = GPIO.input(22)
state3 = GPIO.input(23)
state4 = GPIO.input(27)
state5 = GPIO.input(28)
state6 = GPIO.input(29)


if state1:
	print("Zone 1 is on!")
if state2:
	print("Zone 2 is on!")
if state3:
	print("Zone 3 is on!")
if state4:
	print("Zone 4 is on!")
if state5:
	print("Zone 5 is on!")
if state6:
	print("Zone 6 is on!")

print("End of readGPIO.py")

