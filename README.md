# Irrigation

![irrigation](irrigationPic.JPG)

Control irrigation valves (or anything else) using a Raspberry Pi and a relay module.

### How it works:

- the executable Main (see src/Main.cpp) reads a schedule from disk and turns relays on and off accordingly.

- the Flask app (see Flask directory) allows users to modify the schedule via any internet-connected device.

- the Flask app notifies the Main executable via a named pipe when the schedule changes so that the Main executable can reread the schedule.


I use systemd to run nginx and the Main executable, and supervisord to run Gunicorn (to serve the Flask app) so that everything starts at boot, and restarts if errors occur.

I also use autossh to perform a reverse ssh to my server so that my Pi is accessible from outside my local network.

-----------------------------------------------------------------------------------------------------

### Setup:

Clone the project repository

Outside the project repository, create a python virtual environment

Install python prerequisites:
pip install -r requirements.txt

Install supervisor:
sudo apt-get install supervisor

Navigate to the project repository

Build Main:
g++ src/Main.cpp -o Main -lwiringPi

<br>
Create folder and file for schedules:

mkdir schedules

touch schedules/alternating.txt

<br>
To run the Main executable as a systemd service:

cp conf/irrigation.service /etc/systemd/system

sudo systemctl enable irrigation

sudo systemctl start irrigation

<br>
To run the Flask app as a supervisord service:

cp irrigationFlask.conf /etc/supervisor/conf.d

sudo supervisorctl reread

sudo supervisorctl update

sudo supervisorctl start irrigationFlask

<br>
Set up a log directory for the Flask app:

sudo mkdir /var/log/irrigationFlask


### Other Setup (without details):

Install and setup nginx as a reverse proxy to route requests to Gunicorn

Install and setup autossh for reliable reverse ssh

In nginx conf, allow gateways so autossh can perform reverse ssh

