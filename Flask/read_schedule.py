def read_schedule():
	with open ('/home/pi/Documents/Irrigation/Git/schedules/alternating.txt', 'r') as f:
		lines = f.readlines()

	even = True
	most_recent = None

	schedule_dict = dict()

	for line in lines:
		line = line.strip()
		# print(line)
		if line == 'even':
			even = True
		elif line == 'odd':
			even = False
		else:
			if even:
				# print("even ---")
				if line[-1] == '1':
					schedule_dict['e1s'] = line[:-2]
					most_recent = '1'
				elif line[-1] == '2':
					schedule_dict['e2s'] = line[:-2]
					most_recent = '2'
				elif line[-1] == '3':
					schedule_dict['e3s'] = line[:-2]
					most_recent = '3'
				elif line[-1] == '4':
					schedule_dict['e4s'] = line[:-2]
					most_recent = '4'
				elif line[-1] == '5':
					schedule_dict['e5s'] = line[:-2]
					most_recent = '5'
				elif line[-1] == '6':
					schedule_dict['e6s'] = line[:-2]
					most_recent = '6'
				elif line[-1] == '0':
					if most_recent == '1':
						schedule_dict['e1f'] = line[:-2]
					if most_recent == '2':
						schedule_dict['e2f'] = line[:-2]
					if most_recent == '3':
						schedule_dict['e3f'] = line[:-2]
					if most_recent == '4':
						schedule_dict['e4f'] = line[:-2]
					if most_recent == '5':
						schedule_dict['e5f'] = line[:-2]
					if most_recent == '6':
						schedule_dict['e6f'] = line[:-2]
			else:
				if line[-1] == '1':
					schedule_dict['o1s'] = line[:-2]
					most_recent = '1'
				elif line[-1] == '2':
					schedule_dict['o2s'] = line[:-2]
					most_recent = '2'
				elif line[-1] == '3':
					schedule_dict['o3s'] = line[:-2]
					most_recent = '3'
				elif line[-1] == '4':
					schedule_dict['o4s'] = line[:-2]
					most_recent = '4'
				elif line[-1] == '5':
					schedule_dict['o5s'] = line[:-2]
					most_recent = '5'
				elif line[-1] == '6':
					schedule_dict['o6s'] = line[:-2]
					most_recent = '6'
				elif line[-1] == '0':
					if most_recent == '1':
						schedule_dict['o1f'] = line[:-2]
					if most_recent == '2':
						schedule_dict['o2f'] = line[:-2]
					if most_recent == '3':
						schedule_dict['o3f'] = line[:-2]
					if most_recent == '4':
						schedule_dict['o4f'] = line[:-2]
					if most_recent == '5':
						schedule_dict['o5f'] = line[:-2]
					if most_recent == '6':
						schedule_dict['o6f'] = line[:-2]

	# print(schedule_dict)
	return schedule_dict




if __name__ == '__main__':
	read_schedule()