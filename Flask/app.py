import os, sys
from flask import Flask, render_template, request
from datetime import datetime
from argon2 import PasswordHasher
from argon2.exceptions import VerificationError, VerifyMismatchError, HashingError, InvalidHash
import read_schedule
from forms import ScheduleForm

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def hello_world():

	# check if authenticated or not
	sched = read_schedule.read_schedule()

	form = ScheduleForm(request.form)
	if request.method == 'POST' and form.validate():
		new_sched = request.form.to_dict(flat=False)

		# authenticate
		# form_username = new_sched["input_username"][0]
		# form_password = new_sched["input_password"][0]

		# with open("/home/pi/Documents/Irrigation/username.txt", "r") as u_file:
		# 	disk_username = u_file.read()

		# with open("/home/pi/Documents/Irrigation/password.txt", "r") as pw_file:
		# 	disk_password = pw_file.read()

		# if form_username == disk_username:
		# 	try:
		# 		ph = PasswordHasher()
		# 		ph.verify(disk_password, form_password)
		# 		# print("Verified!")
		# 	except (VerificationError, VerifyMismatchError, HashingError, InvalidHash):
		# 		# print("password error")
		# 		return render_template('index.html', d=sched, error=True)
		# else:
		# 	# print("usernames didn't match")
		# 	return render_template('index.html', d=sched, error=True)


		# enforce that only 1 relay can be open at a time

		# to verify no overlapping times:
		timeListEven = []
		timeListOdd = []

		possibleScheduleEven = []
		possibleScheduleOdd = []

		# finish time must be greater than start time:
		if new_sched['input_e1s'][0] != '' and new_sched['input_e1f'][0] != '':
			if new_sched['input_e1s'][0] < new_sched['input_e1f'][0]:
				timeListEven.append([new_sched['input_e1s'][0], new_sched['input_e1f'][0]])
				possibleScheduleEven.append([new_sched['input_e1s'][0], '1'])
				possibleScheduleEven.append([new_sched['input_e1f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		if new_sched['input_e2s'][0] != '' and new_sched['input_e2f'][0] != '':
			if new_sched['input_e2s'][0] < new_sched['input_e2f'][0]:
				timeListEven.append([new_sched['input_e2s'][0], new_sched['input_e2f'][0]])
				possibleScheduleEven.append([new_sched['input_e2s'][0], '2'])
				possibleScheduleEven.append([new_sched['input_e2f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		if new_sched['input_e3s'][0] != '' and new_sched['input_e3f'][0] != '':
			if new_sched['input_e3s'][0] < new_sched['input_e3f'][0]:
				timeListEven.append([new_sched['input_e3s'][0], new_sched['input_e3f'][0]])
				possibleScheduleEven.append([new_sched['input_e3s'][0], '3'])
				possibleScheduleEven.append([new_sched['input_e3f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		if new_sched['input_e4s'][0] != '' and new_sched['input_e4f'][0] != '':
			if new_sched['input_e4s'][0] < new_sched['input_e4f'][0]:
				timeListEven.append([new_sched['input_e4s'][0], new_sched['input_e4f'][0]])
				possibleScheduleEven.append([new_sched['input_e4s'][0], '4'])
				possibleScheduleEven.append([new_sched['input_e4f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		if new_sched['input_e5s'][0] != '' and new_sched['input_e5f'][0] != '':
			if new_sched['input_e5s'][0] < new_sched['input_e5f'][0]:
				timeListEven.append([new_sched['input_e5s'][0], new_sched['input_e5f'][0]])
				possibleScheduleEven.append([new_sched['input_e5s'][0], '5'])
				possibleScheduleEven.append([new_sched['input_e5f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		if new_sched['input_e6s'][0] != '' and new_sched['input_e6f'][0] != '':
			if new_sched['input_e6s'][0] < new_sched['input_e6f'][0]:
				timeListEven.append([new_sched['input_e6s'][0], new_sched['input_e6f'][0]])
				possibleScheduleEven.append([new_sched['input_e6s'][0], '6'])
				possibleScheduleEven.append([new_sched['input_e6f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)





		if new_sched['input_o1s'][0] != '' and new_sched['input_o1f'][0] != '':
			if new_sched['input_o1s'][0] < new_sched['input_o1f'][0]:
				timeListOdd.append([new_sched['input_o1s'][0], new_sched['input_o1f'][0]])
				possibleScheduleOdd.append([new_sched['input_o1s'][0], '1'])
				possibleScheduleOdd.append([new_sched['input_o1f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		
		if new_sched['input_o2s'][0] != '' and new_sched['input_o2f'][0] != '':
			if new_sched['input_o2s'][0] < new_sched['input_o2f'][0]:
				timeListOdd.append([new_sched['input_o2s'][0], new_sched['input_o2f'][0]])
				possibleScheduleOdd.append([new_sched['input_o2s'][0], '2'])
				possibleScheduleOdd.append([new_sched['input_o2f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		if new_sched['input_o3s'][0] != '' and new_sched['input_o3f'][0] != '':
			if new_sched['input_o3s'][0] < new_sched['input_o3f'][0]:
				timeListOdd.append([new_sched['input_o3s'][0], new_sched['input_o3f'][0]])
				possibleScheduleOdd.append([new_sched['input_o3s'][0], '3'])
				possibleScheduleOdd.append([new_sched['input_o3f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		if new_sched['input_o4s'][0] != '' and new_sched['input_o4f'][0] != '':
			if new_sched['input_o4s'][0] < new_sched['input_o4f'][0]:
				timeListOdd.append([new_sched['input_o4s'][0], new_sched['input_o4f'][0]])
				possibleScheduleOdd.append([new_sched['input_o4s'][0], '4'])
				possibleScheduleOdd.append([new_sched['input_o4f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		if new_sched['input_o5s'][0] != '' and new_sched['input_o5f'][0] != '':
			if new_sched['input_o5s'][0] < new_sched['input_o5f'][0]:
				timeListOdd.append([new_sched['input_o5s'][0], new_sched['input_o5f'][0]])
				possibleScheduleOdd.append([new_sched['input_o5s'][0], '5'])
				possibleScheduleOdd.append([new_sched['input_o5f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)

		if new_sched['input_o6s'][0] != '' and new_sched['input_o6f'][0] != '':
			if new_sched['input_o6s'][0] < new_sched['input_o6f'][0]:
				timeListOdd.append([new_sched['input_o6s'][0], new_sched['input_o6f'][0]])
				possibleScheduleOdd.append([new_sched['input_o6s'][0], '6'])
				possibleScheduleOdd.append([new_sched['input_o6f'][0], '0'])
			else:
				return render_template('index.html', d=sched, error=True)


		# check no overlapping times:
		timeListEven.sort()
		for i in range(1, len(timeListEven)):
			if timeListEven[i][0] < timeListEven[i - 1][1]:
				return render_template('index.html', d=sched, error=True)

		timeListOdd.sort()
		for i in range(1, len(timeListOdd)):
			if timeListOdd[i][0] < timeListOdd[i - 1][1]:
				return render_template('index.html', d=sched, error=True)

		# write the file
		# could make this conditional - only write even or odd if len(possibleSchedulEven or Odd) > 0
		with open('/home/pi/Documents/Irrigation/Git/schedules/alternating.txt', 'w') as f:
			if len(possibleScheduleEven) > 0:
				f.write('even')
				possibleScheduleEven.sort()
				for i in possibleScheduleEven:
					f.write('\n' + i[0] + ' ' + i[1])
			if len(possibleScheduleOdd) > 0:
				f.write('odd')
				possibleScheduleOdd.sort()
				for i in possibleScheduleOdd:
					f.write('\n' + i[0] + ' ' + i[1])



		sched = read_schedule.read_schedule()

		with open("/tmp/irrigationFIFO", "w") as fifo:
			fifo.write("a")

		return render_template('index.html', d=sched, error=False)

	else:
		return render_template('index.html', d=sched)

