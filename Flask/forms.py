from wtforms import Form, TimeField, StringField, PasswordField, validators

class ScheduleForm(Form):
	# username = StringField('input_username')
	# password = PasswordField('input_password')

	even1Start = TimeField('input_e1s')
	even1Finish = TimeField('input_e1f')
	even2Start = TimeField('input_e2s')
	even2Finish = TimeField('input_e2f')
	even3Start = TimeField('input_e3s')
	even3Finish = TimeField('input_e3f')
	even4Start = TimeField('input_e4s')
	even4Finish = TimeField('input_e4f')
	even5Start = TimeField('input_e5s')
	even5Finish = TimeField('input_e5f')
	even6Start = TimeField('input_e6s')
	even6Finish = TimeField('input_e6f')

	odd1Start = TimeField('input_o1s')
	odd1Finish = TimeField('input_o1f')
	odd2Start = TimeField('input_o2s')
	odd2Finish = TimeField('input_o2f')
	odd3Start = TimeField('input_o3s')
	odd3Finish = TimeField('input_o3f')
	odd4Start = TimeField('input_o4s')
	odd4Finish = TimeField('input_o4f')
	odd5Start = TimeField('input_o5s')
	odd5Finish = TimeField('input_o5f')
	odd6Start = TimeField('input_o6s')
	odd6Finish = TimeField('input_o6f')

