from argon2 import PasswordHasher

def create_credentials():
	ph = PasswordHasher()

	username = input("Enter your username: ")
	password = input("Enter your password: ")
	confirm_password = input("Enter your password again to confirm: ")

	if password == confirm_password:

		password_hash = ph.hash(password)

		with open("/home/pi/Documents/Irrigation/username.txt", "w") as u_file:
			u_file.write(username)

		with open("/home/pi/Documents/Irrigation/password.txt", "w") as p_file:
			p_file.write(password_hash)

	print("Username: " + username)
	print("Hash of password: " + password_hash)

if __name__ == "__main__":
	create_credentials()

