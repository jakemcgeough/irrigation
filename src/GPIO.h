#pragma once

#include <vector>

#include "LinuxIncludes.h"
#include "NodeUtil.h"

const std::vector<int> zones {0, 21, 22, 23, 27, 28, 29};

class RelayControlInterface
{
public:
	virtual void ActivateRelay(NodeAction nodeAction) = 0;

protected:
	int currentOnRelay = 0;
	bool anyRelayOn = false;
	virtual void Off() = 0;
};

class RelayControl : public RelayControlInterface
{
public:
	RelayControl();
	void ActivateRelay(NodeAction nodeAction) override;

private:
	void Off() override;
};
