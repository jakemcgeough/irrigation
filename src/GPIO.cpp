#include "GPIO.h"

#include <chrono>
#include <iostream>
#include <thread>

RelayControl::RelayControl()
{
#ifdef __linux__
	wiringPiSetup();
	pinMode(zones[1], OUTPUT);
	pinMode(zones[2], OUTPUT);
	pinMode(zones[3], OUTPUT);
	pinMode(zones[4], OUTPUT);
	pinMode(zones[5], OUTPUT);
	pinMode(zones[6], OUTPUT);

	digitalWrite(zones[1], 1);
	digitalWrite(zones[2], 1);
	digitalWrite(zones[3], 1);
	digitalWrite(zones[4], 1);
	digitalWrite(zones[5], 1);
	digitalWrite(zones[6], 1);
#endif
}

// maybe instead of turning off specific valve, just turn everything off - have separate function that turns all off
void RelayControl::ActivateRelay(NodeAction nodeAction)
{
	if (nodeAction.relayState == RelayState::Off)
	{
		Off();
		anyRelayOn = false;
	}
	else if (anyRelayOn && currentOnRelay != nodeAction.relay)
	{
		Off();
		std::this_thread::sleep_for(std::chrono::seconds(60));
		currentOnRelay = nodeAction.relay;
		anyRelayOn = true;
#ifdef __linux__
		digitalWrite(zones[nodeAction.relay], 0);
#endif
#ifdef _WIN32
		std::cout << "ON: zone " << nodeAction.relay << "\n" << std::endl;
#endif
	}
	else
	{
		currentOnRelay = nodeAction.relay;
		anyRelayOn = true;
#ifdef __linux__
		digitalWrite(zones[nodeAction.relay], 0);
#endif
#ifdef _WIN32
		std::cout << "ON: zone " << nodeAction.relay << "\n" << std::endl;
#endif
	}
}

void RelayControl::Off()
{
#ifdef __linux__
	digitalWrite(zones[1], 1);
	digitalWrite(zones[2], 1);
	digitalWrite(zones[3], 1);
	digitalWrite(zones[4], 1);
	digitalWrite(zones[5], 1);
	digitalWrite(zones[6], 1);
#endif
#ifdef _WIN32
	std::cout << "-- OFF" << "\n" << std::endl;
#endif
}
