#pragma once

#include <string>

struct Node
{
	std::string day {};
	std::string startTime {};
	std::string stopTime {};
	int zone {};
};
