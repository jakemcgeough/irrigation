#include "ScheduleManager.h"

#include <fstream>

// if reading fails, clear nodeList and return - no relays will be activated
// if schedule unchanged, return early
void ScheduleManager::ReadSchedule()
{
	std::ifstream scheduleFile(schedulePath);
	nlohmann::json newSchedule = nlohmann::json::parse(scheduleFile);
	scheduleType = (newSchedule["days"][0]["day"] == "even" || newSchedule["days"][0]["day"] == "odd") ? ScheduleType::Alternating : ScheduleType::Daily;
	if (newSchedule == schedule || scheduleType == ScheduleType::Daily) // schedule unchanged / scheduleType is daily (not implemented)
	{
		return; // ?
	}

	nodeManager.ResetNodeList();

	const bool dateEven = timeManager.GetCurrentDateEven();

	Node currentNode{};

	const auto now = timeManager.GetNow();
	const std::chrono::year_month_day ymd{std::chrono::floor<std::chrono::days>(now.get_local_time())};

	const std::string dayString = [&] {
		if (scheduleType == ScheduleType::Alternating)
		{
			if (dateEven)
			{
				return "even";
			}
			else
			{
				return "odd";
			}
		}
		else
		{
			return "";
		}
	}();

	const std::string nowString = std::format("{:%H:%M}", now);

	// parse json schedule into nodes
	for (auto& day : newSchedule["days"])
	{
		for (auto& entry : day["entries"])
		{
			bool addNodeSuccess = nodeManager.AddNode(Node { .day = day["day"], .startTime = entry["start"], .stopTime = entry["stop"],.zone = entry["zone"] },
													  dayString,
													  nowString);

			if (!addNodeSuccess)
			{
				nodeManager.ResetNodeList();
				return; // ?
			}
		}
	}
}

ScheduleType& ScheduleManager::GetScheduleType()
{
	return scheduleType;
}
