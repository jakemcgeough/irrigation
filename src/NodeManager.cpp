#include "NodeManager.h"

bool NodeManager::AddNode(Node node, std::string_view dayString, std::string_view timeString)
{
	if (!ValidNode(node) && dayString != "even" && dayString != "odd")
	{
		return false;
	}

	nodeList.push_back(node);

	if (node.day == dayString && timeString <= node.stopTime)
	{
		currentIndex = nodeList.size() - 1;
	}
	// if no entries for current day, currentIndex will remain 0, which is correct
	return true;
}

NodeAction NodeManager::GetCurrentNodeAction(std::string_view dayString, std::string_view timeString)
{
	while (nodeList[currentIndex].day == dayString && nodeList[currentIndex].stopTime < timeString && (nodeList[currentIndex].startTime < nodeList[GetNextIndex()].startTime))
	{
		if (currentIndex == (nodeList.size() - 1))
		{
			currentIndex = 0;
		}
		else
		{
			currentIndex++;
		}
	}

	if (dayString == nodeList[currentIndex].day && timeString > nodeList[currentIndex].startTime && timeString < nodeList[currentIndex].stopTime)
	{
		return { nodeList[currentIndex].zone, RelayState::On };
	}
	else
	{
		return { 0, RelayState::Off };
	}
}

void NodeManager::ResetNodeList()
{
	nodeList.clear();
	currentIndex = 0;
}

bool NodeManager::ValidNode(Node& node)
{
	// invalid zone
	if (node.zone < 0 || node.zone > 6)
	{
		return false;
	}

	// invalid start / stop time
	if (node.startTime > node.stopTime)
	{
		return false;
	}

	// start time overlaps previous stop time
	if (nodeList.size() > 0 && node.startTime < nodeList[nodeList.size() - 1].stopTime)
	{
		return false;
	}

	return true;
}

std::size_t NodeManager::GetNextIndex() const
{
	if (currentIndex == nodeList.size() - 1)
	{
		return 0;
	}
	else
	{
		return currentIndex + 1;
	}
}
