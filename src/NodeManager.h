#pragma once

#include <string_view>
#include <vector>

#include "Node.h"
#include "NodeUtil.h"

class NodeManager
{
public:
	bool AddNode(Node node, std::string_view dayString, std::string_view timeString);
	NodeAction GetCurrentNodeAction(std::string_view dayString, std::string_view timeString);
	void ResetNodeList();

private:
	std::vector<Node> nodeList {};
	std::size_t currentIndex {}; // index of current node in nodeList
	bool ValidNode(Node& node);
	std::size_t GetNextIndex() const;
};
