#pragma once

enum class RelayState
{
	On,
	Off
};

struct NodeAction
{
	int relay {};
	RelayState relayState{};
};
