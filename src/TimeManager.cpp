#include "TimeManager.h"

// would be better to keep track of which day we started
// then alternate from there
bool TimeManager::GetCurrentDateEven()
{
	const auto now = GetNow();
	const std::chrono::year_month_day ymd{std::chrono::floor<std::chrono::days>(now.get_local_time())};
	const auto day = static_cast<unsigned int>(ymd.day());
	return (day % 2 == 0);
}

std::chrono::zoned_time<std::chrono::system_clock::duration, const std::chrono::time_zone*> TimeManager::GetNow()
{
	return std::chrono::zoned_time{std::chrono::current_zone(), std::chrono::system_clock::now()};
}
