#include <chrono>
#include <iostream>
#include <format>
#include <string>
#include <thread>

#include "nlohmann/json.hpp"

#include "GPIO.h"
#include "LinuxIncludes.h"
#include "node.h"
#include "ScheduleManager.h"
#include "TimeManager.h"

using namespace std::chrono_literals;

// hard limit of 6 irrigation zones (i.e. 6 relays, 6 valves)
// only one valve open at a time
// 1 minute delay between - whenever opening, wait 1 minute, then open

// TODO:
// enforce same day start-stop for simplicity

// current time should be >= current node time
// current node can be either start or stop
// check that current time is still less than next node time
// when current time is >= next node, move to next node

// sequence must be: start -> stop -> start -> stop ....
// if 2 starts or stops in a row, error

int main()
{	
	TimeManager timeManager;
	RelayControl relayControl;
	NodeManager nodeManager;
	ScheduleManager scheduleManager("C:/Users/Jake/Documents/Code/Irrigation/Git/schedules/schedule.json", timeManager, nodeManager);

	while (true)
	{
		scheduleManager.ReadSchedule();

		const auto now = timeManager.GetNow();
		const std::string nowString = std::format("{:%H:%M}", now.get_local_time());

		std::string dayString = timeManager.GetCurrentDateEven() ? "even" : "odd";

		relayControl.ActivateRelay(nodeManager.GetCurrentNodeAction(dayString, nowString));

		auto nextMinute = std::chrono::floor<std::chrono::minutes>(timeManager.GetNow().get_sys_time() + std::chrono::duration_cast<std::chrono::minutes>(60s));
		std::this_thread::sleep_until(nextMinute);
	}

	return 0;
}

// or maybe invert nodeManager and scheduleManager
// nodeManager's constructor calls ReadSchedule
// and then in the loop we call nodeManager UPdateSchedule or sth, which gets the schedule via ReadSchedule

// maybe sth like GetNode() in a loop to get all nodes in the schedule? if the schedule has changed
