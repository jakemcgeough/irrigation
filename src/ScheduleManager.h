#pragma once

#include <string>

#include "nlohmann/json.hpp"

#include "Node.h"
#include "NodeManager.h"
#include "TimeManager.h"

enum class ScheduleType
{
	Daily,
	Alternating
};

class ScheduleManager
{
public:
	ScheduleManager(std::string schedulePath, TimeManager& timeManager, NodeManager& nodeManager) : schedulePath{schedulePath}, timeManager{timeManager}, nodeManager{nodeManager} {};
	void ReadSchedule();
	ScheduleType& GetScheduleType();

private:
	const std::string schedulePath {};
	TimeManager& timeManager;
	NodeManager& nodeManager;
	nlohmann::json schedule {}; // raw json schedule
	ScheduleType scheduleType = ScheduleType::Alternating;
};
