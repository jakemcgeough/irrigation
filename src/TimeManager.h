#pragma once

#include <chrono>

class TimeManagerInterface
{
public:
	virtual bool GetCurrentDateEven() = 0;
	virtual std::chrono::zoned_time<std::chrono::system_clock::duration, const std::chrono::time_zone*> GetNow() = 0;
};

class TimeManager : public TimeManagerInterface
{
public:
	bool GetCurrentDateEven() override;
	std::chrono::zoned_time<std::chrono::system_clock::duration, const std::chrono::time_zone*> GetNow() override;
};
